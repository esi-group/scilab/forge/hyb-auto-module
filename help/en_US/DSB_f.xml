<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) DIGITEO - 2010 - Ievgen IVANOV <ivanov.eugen@gmail.com>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 -->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:id="DSB_f">
  <refnamediv>
    <refname>DSB_f</refname>
    <refpurpose>Discrete-time state block</refpurpose>
  </refnamediv>
  <refsection>
    <title>Block Screenshot</title>
    <para>
      <inlinemediaobject>
        <imageobject>
          <imagedata fileref="../../images/DSB_f.png" align="center" valign="middle"/>
        </imageobject>
      </inlinemediaobject>
    </para>
  </refsection>
  <refsection id="Contents_DSB_f">
    <title>Contents</title>
    <itemizedlist>
      <listitem>
        <para>
          <link linkend="DSB_f">Discrete-time state block</link>
        </para>
      </listitem>
      <listitem>
        <itemizedlist>
          <listitem>
            <para>
              <xref linkend="Palette_DSB_f">Palette</xref>
            </para>
          </listitem>
		  <listitem>
            <para>
              <xref linkend="Description_DSB_f">Description</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Dialogbox_DSB_f">Dialog box</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Defaultproperties_DSB_f">Default properties</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Interfacingfunction_DSB_f">Interfacing function</xref>
            </para>
          </listitem>
        </itemizedlist>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Palette_DSB_f">
    <title>Palette</title>
    <itemizedlist>
      <listitem>
        <para>
          <link linkend="hyb_auto_module_pal">Hybrid Automata Module palette</link>
        </para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Description_DSB_f">
    <title>Description</title>
    <para>
	This block represents a discrete state (mode) with dynamics governed by
	(discrete-time) difference equations in hybrid automaton's state diagram.
	It should be used only in state diagrams specified with the help of HAS_f block.
	</para>
  </refsection>
  <refsection id="Dialogbox_DSB_f">
    <title>Dialog box</title>
    <para>
      <inlinemediaobject>
        <imageobject>
          <imagedata fileref="../../images/DSB_f_gui.png" align="center" valign="middle"/>
        </imageobject>
      </inlinemediaobject>
    </para>
    <para>

</para>
    <itemizedlist>
      <listitem>
        <para>
          <emphasis role="bold">Number of in-ports</emphasis>
        </para>
        <para> Number of input ports, which can be used to specify transitions
		into this state</para>
        <para> Properties : Type 'vec' of size 1. </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">Number of out-ports</emphasis>
        </para>
        <para> Number of output ports, which can be used to specify transitions
		from this state</para>
        <para> Properties : Type 'vec' of size 1. </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">difference equation RHS</emphasis>
        </para>
        <para> Right hand sides of difference equations, which govern dynamics in
		this state. This parameter should be given in the form of column vector of
		string expressions in variables u1,u2,...uN which represent continuous
		state of hybrid automaton. The i-th string contains expression of right
		hand side of autonomous difference equation with left hand side ui(t+h),
		where h is a time delta (see below). For example, RHS vector ['-u2';'-u1']
		specifies difference equations u1(t+h)=-u2(t), u2(t+dt)=-u1(t).
		If "Input size" parameter of HAS_f block is non-zero, then you can use
		use input control variables v1,v2,... in RHS expressions.
		These variables represent current values on input port of HAS_f block.
		For example, RHS vector ['v1-u1'] specifies difference
		equation u1(t+h)=v1(t)-u1(t) with input control v1(t). </para>
        <para> Properties : Type 'str' of size -1. </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">Is initial (0/1)</emphasis>
        </para>
        <para> Specifies whether state is initial (1 if initial).</para>
        <para> Properties : Type 'vec' of size 1. </para>
      </listitem>
	  <listitem>
        <para>
          <emphasis role="bold">Time delta</emphasis>
        </para>
        <para> Specifies time step for difference equations.</para>
        <para> Properties : Type 'vec' of size 1. </para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Defaultproperties_DSB_f">
    <title>Default properties</title>
    <itemizedlist>
      <listitem>
        <para><emphasis role="bold">always active:</emphasis> no</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">direct-feedthrough:</emphasis> no</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">zero-crossing:</emphasis> no</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">mode:</emphasis> no</para>
      </listitem>
	  <listitem>
        <para>
          <emphasis role="bold">regular inputs:</emphasis>
        </para>
        <para>
          <emphasis role="bold">- port 1 : size [1,1] / type 1</emphasis>
        </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">regular outputs:</emphasis>
        </para>
        <para>
          <emphasis role="bold">- port 1 : size [1,1] / type 1</emphasis>
        </para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">number/sizes of activation inputs:</emphasis> 0</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">number/sizes of activation outputs:</emphasis> 0</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">continuous-time state:</emphasis> no</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">discrete-time state:</emphasis> no</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">object discrete-time state:</emphasis> no</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Interfacingfunction_DSB_f">
    <title>Interfacing function</title>
    <itemizedlist>
      <listitem>
        <para> hyb-auto-module/macros/DSB_f.sci</para>
      </listitem>
    </itemizedlist>
  </refsection>  
</refentry>
