/*  Scicos
*
*  Copyright (C) INRIA - METALAU Project <scicos@inria.fr>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*
* See the file ./license.txt
*/
#include "scicos_block4.h"
#include <math.h>

/* automat2 is a modified version of AUTOMAT computational function defined in
	 modules/scicos_blocks/src/c/automat.c
	 Switching between subsystems in AUTOMAT block occurs on events of crossing of specified
	 surfaces. This approach works when subsystem describes continuous evolution,
	 but is not applicable in the case of presence of discontinuities,
	 e.g. if subsystem models dynamics of difference equation obtained from DSB state.
	 This problem is solved in automat2 by means of introduction of additional type of
	 jump conditions. In automat2 jump conditions can be of 3 types:
	 1) zero-crossing jump condition: jump when the value of the given continuous function
	 changes sign from - to +
	 2) invariant jump condition: jump when the value of the given (possibly discontinuous)
	 function becomes positive
	 3) periodic auto-jump: periodically jump from state to itauto
	 (useful for modeling of states with dynamics decribed by difference equations)
	 The type of jump condition is indicated by sign and value of the index state in the
	 jump-to vector Zi (notation taken from AUTOMAT documentation).
	 The sign is "+" (and the value<>100000) for jumps of the type 1, sign is "-" for
	 jumps of the type 2 and the value is 100000 for jumps of the type 3.
	 Automat computational function block supports only jumps of the 1st type.
	 In other aspects behavior of automat2 is the same as automat.
	 */

void automat2(scicos_block *block, int flag)
{
	double *y0, *y1, *ui;
	double* g=block->g;
	double* x=block->x;
	double* xd=block->xd;
	double* res=block->res;
	void** work=block->work;
	double* rpar=block->rpar;
	double* evout=block->evout;

	int* ipar=block->ipar;
	int* jroot=block->jroot;
	int nevprt=block->nevprt;
	int* insz=block->insz;
	int ng=block->ng;

	int NMode, NX, Minitial, i,j,k, Mi, Mf, indice;
	int* Mode, *property, *iparXp, *iparCx;
	double* rparX0, *last_t;
	int PerautoJump=100000; /* indicator of periodic auto-jump */

	NMode=ipar[0];
	Minitial=ipar[1];
	NX=ipar[2];
	iparXp=ipar+3;
	iparCx=iparXp+NX*NMode;
	rparX0=rpar;

	if (flag ==4) {	/*---------------------------------------------------------------*/
		if ((*work = scicos_malloc(sizeof(int)*(2+NX)+sizeof(double))) == NULL) {
			set_block_error(-16);
			return;
		}
		Mode=*work;
		property=Mode+2;
		Mode[0]=Minitial; /* Current Mode; */
		Mode[1]=Minitial; /* Previous Mode */
		for (i=0;i<NX;i++) property[i]=0; /* xproperties */
		for (i=0;i<NX;i++) x[i]=rparX0[i];
		last_t=(double*)(Mode+2+NX); /* last time of auto-jump */
		*last_t=get_scicos_time();

	} else if (flag ==5) { /**-------------------------------------------------------*/
		scicos_free(*work);

	} else if (flag ==1 || flag ==6) {	/*-------------------------------------------*/
		y0=GetRealOutPortPtrs(block,1);
		y1=GetRealOutPortPtrs(block,2);

		Mode=*work;
		Mi=Mode[0];
		y0[0]=Mi; /* current Mode */
		y0[1]=Mode[1]; /* prevous Mode */
		for (i=0;i<NX;i++) {
			y1[i]=x[i];
			y1[i+NX]=xd[i];
		}

	} else if (flag==0) { /*---------------------------------------------------------*/
		Mode=*work;
		Mi=Mode[0];
		indice=0; Mf=Mi;

		ui=GetRealInPortPtrs(block,Mi);
		for (i=1;i<Mi;i++)
			indice+=insz[i-1]-2*NX; /* number of modes before Mi_th Mode */
		for (k=0;k<insz[Mi-1]-2*NX;k++) { /* check jumps of type 2 */
			Mf=-iparCx[indice+k];
			if (Mf>0 && ui[k+2*NX]>0) { /* switch only if jump condition has type 2 */
				Mode[0]=Mf; /* saving the new Mode */
				Mode[1]=Mi; /* saving the previous Mode */
				ui=GetRealInPortPtrs(block,Mf);
				for (i=0;i<NX;i++)
					x[i]=ui[i+NX]; /* reinitialize the states */
				break;
			}
		}
		for (i=0;i<NX;i++)
			res[i]=ui[i];

	} else if (flag==7) { /*---------------------------------------------------------*/
		Mode=*work;
		property=Mode+2;
		Mi=Mode[0];
		for (i=0;i<NX;i++)
			property[i] = iparXp[(Mi-1)*NX+i];
		set_pointer_xproperty(property);

	} else if (flag==9) { /*--------------------------------------------------------*/
		Mode=*work;
		Mi=Mode[0];
		indice=0;
		last_t=(double*)(Mode+2+NX);
		ui=GetRealInPortPtrs(block,Mi);
		for (i=1;i<Mi;i++)
			indice+=insz[i-1]-2*NX;

		for (j=0;j<ng;j++) g[j]=0;
		for (j=0;j<insz[Mi-1]-2*NX;j++) {
			/* periodic auto-jump, ui[j+2*NX] = frequency */
			if (iparCx[indice+j]==PerautoJump)
				g[j]=sin(ui[j+2*NX]*(get_scicos_time()-(*last_t)));
			else g[j]=ui[j+2*NX];
		}

	} else if ((flag==3)&&(nevprt<0)) { /*-------------------------------------------*/
		Mode=*work;
		Mi=Mode[0];
		indice=0;
		for (i=1;i<Mi;i++)
			indice+=insz[i-1]-2*NX; /* number of modes before Mi_th Mode */
		for (k=0;k<insz[Mi-1]-2*NX;k++)
			if(jroot[k]==1){
				evout[0]=0.0;
				break;
			}

	} else if ((flag==2)&&(nevprt<0)) { /*-------------------------------------------*/
		Mode=*work;
		Mi=Mode[0];
		indice=0;
		last_t=(double*)(Mode+2+NX);
		/* in case where the user has defined a wrong mode destination or ZC direction. */
		Mf=Mi;
		for (i=1;i<Mi;i++)
			indice+=insz[i-1]-2*NX; /* number of modes before Mi_th Mode */

		for (k=0;k<insz[Mi-1]-2*NX;k++) {
			if(jroot[k]==1){
				Mf=iparCx[indice+k];
				if (Mf>=0) { /* switch only if jump condition has type 1 */
					/* Special case: periodic auto-jump */
					if (Mf==PerautoJump) {
						if (get_scicos_time()-*last_t<1e-8) continue;
						Mf=Mi;
					} else {
						Mode[0]=Mf; /* saving the new Mode */
						Mode[1]=Mi; /* saving the previous Mode */
					}
					*last_t=get_scicos_time(); /* reset last jump time */
					ui=GetRealInPortPtrs(block,Mf);
					/*reinitialize the states only if type 1 jump occured*/
					for (i=0;i<NX;i++)
						x[i]=ui[i+NX];
					break;
				}
			}
		}
	}
}
