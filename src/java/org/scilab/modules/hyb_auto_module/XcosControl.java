/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - DIGITEO - Ievgen IVANOV
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

package org.scilab.modules.hyb_auto_module;

import java.io.*;
import java.util.List;
import java.lang.reflect.InvocationTargetException;

import javax.swing.SwingUtilities;

import org.scilab.modules.xcos.Xcos;
import org.scilab.modules.xcos.block.BasicBlock;
import org.scilab.modules.xcos.graph.XcosDiagram;

/**
 * Xcos control operations
 *
 * This class invokes Xcos operations.
 */
public class XcosControl {

	/**
	 * Open block settings by uid.
	 * @param uid Block id
	 */
	public static void xcosOpenBlockSettings(String uid) {
	final String id = uid;

	try{
		SwingUtilities.invokeAndWait(new Runnable() {
		public void run() {
			BasicBlock block = null;
			List<XcosDiagram> allDiagrams = Xcos.getInstance().getDiagrams();
			for (XcosDiagram diagram : allDiagrams) {
				block = diagram.getChildById(id);
				if (block != null) {
					block.openBlockSettings(diagram.getScicosParameters().getContext());
					break;
				}
			}
		}
		});
	} catch (InterruptedException e) {
		e.printStackTrace();
	}
	catch (InvocationTargetException e) {
		e.printStackTrace();
	}

	}

	/**
	 * This function converts a HDF5 file to Xcos file
	 * @param h5File The h5 diagram file
	 * @param xcosFile The target file
	 * @param forceOverwrite Does the file will be overwritten ?
	 * @return Non-zero if forceOverwrite is false and file already exists
	 */
	public static int xcosHDF5ToDiagram(String h5File, String xcosFile,
		boolean forceOverwrite) {

	final String file = h5File;
	final File temp = new File(xcosFile);
	final boolean overwrite = forceOverwrite;

	if (h5File.equals(""))
		return 0;

	if (temp.exists()) {
		if (!overwrite)
			return 1;
		else
			temp.delete();
	}

	try {
		SwingUtilities.invokeAndWait(new Runnable() {
		public void run() {
			XcosDiagram diagram = new XcosDiagram();
			diagram.openDiagramFromFile(file);
			diagram.saveDiagramAs(temp.getAbsolutePath());
		}
		});
	} catch (InterruptedException e) {
		e.printStackTrace();
	} catch (InvocationTargetException e) {
		e.printStackTrace();
	}
	return 0;
	}

}
