// Copyright (C) 2010 - Ievgen IVANOV
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

ilib_verbose(2);

XcosCtl_names = ["hybm_xcosOpenBlock" , "sci_xcosOpenBlock"     ;
                "hybm_xcosHDF5ToDiagram"     , "sci_xcosHDF5ToDiagram"          ];

XcosCtl_files = ["sci_xcosOpenBlock.cpp" ;
                "sci_xcosHDF5ToDiagram.cpp"      ;
                "XcosControl.cpp"                ;
                "GiwsException.cpp"              ];

XcosCtl_gateway_path = get_absolute_file_path("builder_gateway_cpp.sce");

java_home=getenv("JAVA_HOME");
//use function getos() from SEP 39
if (getos()=="Windows") then
	cflags = '-I'+java_home+"\include"+" -I"+java_home+"\include\win32";
elseif (getos()=="Linux") then
	cflags = "-I"+java_home+"/include"+" -I"+java_home+"include/linux";
else
	cflags = "-I"+java_home+"/include";
end

tbx_build_gateway("XcosCtl",..
                  XcosCtl_names,..
                  XcosCtl_files,..
                  XcosCtl_gateway_path,..
                  [],..
                  "",..
                  cflags);

clear tbx_build_gateway;
