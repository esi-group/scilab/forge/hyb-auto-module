// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) DIGITEO - 2010 - Ievgen IVANOV <ivanov.eugen@gmail.com>
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


//btdialog_setsig(arg, tag) - set "signal state" (from button handler), i.e. request some
//operation associated with data arg to be performed outside handler
function r=btdialog_setsig(arg, tag)
if argn(2)<2 then
	tag="_btdialog_"
end
h=findobj("tag",tag)
if h==[] then
	r=%f
else
	dat=get(h,"userdata")
	dat(1)=arg
	set(h,"userdata",dat)
	r=%t
end
endfunction
