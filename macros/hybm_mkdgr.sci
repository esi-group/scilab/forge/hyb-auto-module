// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) DIGITEO - 2010 - Ievgen IVANOV <ivanov.eugen@gmail.com>
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


//hybm_mkdgr function makes a diagram (scs_m structure) from blocks and connections
//description lists
//blockls - blocks descriptions:
//  list(list("<blockname>",[x,y],"<blocktype>",<optional arguments>),...);
//  negative x means block flip (doesn"t work until bugs #7257,#7043 aren"t fixed)
//connls - (explicit) connections descriptions: list(list("<src_block_name>",src_port,"<dest_block_name>",dest_port),...)
//  positive values of src_port/dest_port mean (regular) explicit ports, negative values
//  mean event ports
function dgr=hybm_mkdgr(blockls, connls)
if argn(2)<>2 then
	error(msprintf(gettext("%s: Wrong number of input arguments: %d expected.\n"),"hybm_mkdgr",2))
end
if type(blockls)<>15 then
	error(msprintf(gettext("%s: Wrong type for input argument #%d: List expected.\n"),"hybm_mkdgr",1));
end
if type(connls)<>15 then
	error(msprintf(gettext("%s: Wrong type for input argument #%d: List expected.\n"),"hybm_mkdgr",2));
end
dgr=scicos_diagram(version=get_scicos_version())
m=size(blockls)
for i=1:m do //process blockls
	s="";
	for j=4:size(blockls(i)) do
		if j>4 then s=s+",",end
		s=s+"blockls(i)("+sci2exp(j)+")"
	end
	if s=="," then s="", end
	//call an appropriate handler to create a block from its description
	//(list("<blockname>",...))
	execstr("blk=hybm_Mk"+blockls(i)(3)+"("+s+")")
	orig=blockls(i)(2)
	if orig<>[] then
		blk.graphics.orig=abs(orig) //set blocks position
		if orig(1)<0 then blk.graphics.flip=%t, end
	end
	dgr.objs(i)=blk;
	execstr(blockls(i)(1)+"="+sci2exp(i)) //save a block under name, given in description
end
n=size(connls)
for i=1:n do //process connls
	s=evstr(connls(i)(1)), sp=connls(i)(2), d=evstr(connls(i)(3)), dp=connls(i)(4)
	if (sp<0)|(dp<0) then //negative port values mean event ports
		dgr.objs(m+i)=scicos_link(ct=[1,-1],from=[s,abs(sp),0],to=[d,abs(dp),1])
	else //positive port values mean (regular) explicit ports
		dgr.objs(m+i)=scicos_link(ct=[1,1],from=[s,abs(sp),0],to=[d,abs(dp),1])
	end
	if sp>=0 then
		dgr.objs(s).graphics.pout(sp)=m+i //links have numbers m+1,m+2,... (after blocks)
	else
		dgr.objs(s).graphics.peout(sp)=m+i
	end
	if dp>=0 then
		dgr.objs(d).graphics.pin(dp)=m+i
	else
		dgr.objs(d).graphics.pein(-dp)=m+i
	end
end
endfunction

//The following handlers are implemented similary to corresponding block interfacing functions

//handler for expression blocks; vars-variable names, expr-expression
function blk=hybm_MkExpr(vars, expr)
blk=EXPRESSION("define")
[model,graphics,ok]=check_io(blk.model,blk.graphics,-1,1,[],[])
deff("%foo("+vars+")",expr)
[ok,%ok1,ipar,rpar,nz]=compiler_expression(%foo) //supports scicos context
if ~ok | ~%ok1 then
	error("Invalid expression: "+expr)
end
model.rpar=rpar
model.ipar=ipar
model.nzcross=nz
model.nmode=nz
graphics.exprs=["1";expr;"1"]
graphics.sz=[70 40]
blk.model=model
blk.graphics=graphics
endfunction

//handler for muliplexor; Ns - number of inputs
function blk=hybm_MkMux(Ns)
blk=MUX("define")
[model,graphics,ok]=check_io(blk.model,blk.graphics,-[1:Ns]',0,[],[])
model.ipar=Ns
graphics.exprs=sci2exp(Ns)
graphics.sz=[40 40]
blk.model=model
blk.graphics=graphics
endfunction

//handler for extractor; inds - vector of indices to extract
function blk=hybm_MkExtr(inds)
blk=EXTRACTOR("define")
[model,graphics,ok]=check_io(blk.model,blk.graphics,[-1],size(inds,1),[],[])
model.ipar=inds
graphics.exprs=sci2exp(inds)
graphics.sz=[50 40]
blk.model=model
blk.graphics=graphics
endfunction

//handler for splitter
function blk=hybm_MkSplit()
blk=SPLIT_f("define")
endfunction

//handler for subsystem input block
function blk=hybm_MkIn(port)
blk=IN_f("define")
blk.graphics.exprs=sci2exp(port)
blk.graphics.sz=[40 40]
blk.model.ipar=port
endfunction

//handler for subsystem output block
function blk=hybm_MkOut(port)
blk=OUT_f("define")
blk.graphics.exprs=sci2exp(port)
blk.graphics.sz=[40 40]
blk.model.ipar=port
endfunction

//handler for AUTOMAT block
//N - number of modes
//M - (continuous) state size
//transls - list of transitions list(w_1,w_2,...,w_N), where w_i - column vector of modes
//(integers), to which automaton can switch from i-th mode
//init - initial mode
//y0 - initial state
//vers - optional version of AUTOMAT block (default 1 - AUTOMAT, 2 - AUTOMAT2)
function blk=hybm_MkAutomat(N, M, transls, init, y0, vers)
if argn(2)<6 then
	vers=""
elseif int(vers)==1 then
	vers=""
else
	vers=string(int(vers))
end
execstr("blk=AUTOMAT"+vers+"(""define"")")
XP=ones(N,M)
ipar=[N;init;M;matrix(XP',N*M,1)]
exprs=[string(N);string(init);string(M);sci2exp(y0);sci2exp(XP(1,:))]
ins=ones(N,1)
outs=[2;2*M]
nzcross=0
for i=1:size(transls) do
	ipar=[ipar;transls(i)]
	exprs=[exprs;sci2exp(transls(i))]
	leni=length(transls(i))
	ins(i,1)=2*M+leni
	if nzcross<leni then
		nzcross=leni
	end
end
[model,graphics,ok]=check_io(blk.model,blk.graphics,ins,outs,[],[1])
model.nzcross=nzcross
model.state=ones(2*M,1)
model.ipar=ipar
model.rpar=y0
graphics.exprs=exprs
blk.model=model
graphics.sz=[60 40]
blk.graphics=graphics
endfunction

//handler for superblock, ins - inputs number/vector, outs - outputs number/vector,
//scs_m - diagram behind superblock
function blk=hybm_MkSuper(ins, outs, scs_m)
blk=SUPER_f("define")
model=blk.model
model.in=ins
model.out=outs
model.rpar=scs_m
blk=standard_define([2 2],model,[],blk.graphics.gr_i)
blk.gui="SUPER_f"
blk.graphics.sz=[40 40]
endfunction

//handler for 3-input switch
//rule=0: pass first input if u2>=a
//rule=1: pass first input if u2>a
//rule=2: pass first input if u2~=a
function blk=hybm_MkSwitch(rule,a)
blk=SWITCH2_m("define")
blk.graphics.exprs=[sci2exp(1);string(rule);string(a);string(1)]
blk.graphics.sz=[40 40]
blk.model.ipar=rule
blk.model.rpar=a
endfunction

//handler for 2-input substractor
function blk=hybm_MkSubst()
blk=SUMMATION("define") //default parameters make subtraction (not summation)
blk.graphics.sz=[40 40]
endfunction

//handler for periodic signal of magnitude 1, delta_t - signal period
function blk=hybm_MkPeriodicSig(delta_t)
blk=GENSIN_f("define")
rpar=[1;1/delta_t;0]
blk.model.rpar=rpar
blk.graphics.exprs=[string(rpar(1));string(rpar(2));string(rpar(3))]
blk.graphics.sz=[50 40]
endfunction

//handler for constant source block (with value C)
function blk=hybm_MkConst(C)
blk=CONST_f("define")
blk.model.rpar=C(:)
blk.model.out=size(C,"*")
blk.graphics.exprs=strcat(sci2exp(C))
blk.graphics.sz=[40 40]
endfunction
