// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) DIGITEO - 2010 - Ievgen IVANOV <ivanov.eugen@gmail.com>
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


//fn_=hybm_save_name(fn) - Generate a (free) file name _fn, close to fn,
//which can be used for diagram writing; optional string ext specifies extension
function fn_=hybm_save_name(fn,ext)
if argn(2)<>1 & argn(2)<>2 then
	error(msprintf(gettext("%s: Wrong number of input arguments: %d to %d expected.\n"),"hybm_save_name",1,2))
end
if type(fn)<>10 then
	error(msprintf(gettext("%s: Wrong type for input argument #%d: Single string expected.\n"),"hybm_save_name",1));
end
if size(fn,"*")<>1 then
	error(msprintf(gettext("%s: Wrong size for input argument #%d: Single string expected.\n"),"hybm_save_name",1));
end
if argn(2)>1 then
	if type(ext)<>10 then
		error(msprintf(gettext("%s: Wrong type for input argument #%d: Single string expected.\n"),"hybm_save_name",2));
	end
	if size(ext,"*")<>1 then
		error(msprintf(gettext("%s: Wrong size for input argument #%d: Single string 	expected.\n"),"hybm_save_name",2));
	end
end
[start,end_,id]=regexp(fn,"/[0-9]+$/")
if start==[] then
	basefn=fn
	i=0
else
	i=evstr(id)
	basefn=part(fn,1:(start-1))
end
if argn(2)<2 then
	if writeXcosDiagram() then
		ext=".xcos"
	else
		ext=".h5"
	end
end
tst=[]
while %t do	
	fn_=basefn+sci2exp(i)
	//try export_to_hdf5 because mopen does not give reliable result
	err=execstr("export_to_hdf5("+sci2exp(TMPDIR+filesep()+fn_+ext)+","+sci2exp("tst")+")","errcatch")
	if err==0 then
		mdelete(TMPDIR+filesep()+fn_+ext)
		return
	end
	i=i+1
end
endfunction
