// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) DIGITEO - 2010 - Ievgen IVANOV <ivanov.eugen@gmail.com>
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


//[scs_bk,ierr]=hybm_transf_diagr(scs_st) - transform state diagram to block diagram
//scs_st - state diagram in the form of scs_m structure (composed of CSB,DSB,TGB blocks)
//scs_bk - resulting block diagram in the form of scs_m structure
//ierr - error value (0 on success)
function [scs_bk,ierr]=hybm_transf_diagr(scs_st)
if argn(2)<>1 then
	error(msprintf(gettext("%s: Wrong number of input arguments: %d expected.\n"),"hybm_transf_diagr",1))
end
if type(scs_st)<>17 then
	error(msprintf(gettext("%s: Wrong type for input argument #%d: scs_m structure expected.\n"),"hybm_transf_diagr",1));
end
scs_bk=list()
ierr=0
if size(scs_st.objs)==0 then //empty diagram
	scs_bk=scicos_diagram(version=get_scicos_version())
else
	disp("Parsing state diagram...")
	[st_eqls,transls,jcondls,init,y0,insz,ierr]=hybm_parse(scs_st) //parse state diagram
	if ierr<>0 then return, end
	disp("Generating block diagram...")
	[blockls,connls]=hybm_gen_maindgr(st_eqls,transls,jcondls,init,y0,insz)
	scs_bk=hybm_mkdgr(blockls,connls) //produce block diagram
end
scs_bk.props.title="Produced block diagram"
endfunction
