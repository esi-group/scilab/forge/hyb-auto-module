// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) DIGITEO - 2010 - Ievgen IVANOV <ivanov.eugen@gmail.com>
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


//btdialog_setdarg(tag) - set current value of dynamic argument, associated with dialog
//tag - optional string which identifies dialog
function btdialog_setdarg(darg, tag)
if argn(2)<2 then
	tag="_btdialog_"
end
h=findobj("tag",tag)
if h<>[] then
	dat=get(h,"userdata")
	dat(2)=darg
	set(h,"userdata",dat)
end
endfunction
