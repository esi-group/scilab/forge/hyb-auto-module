// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) DIGITEO - 2010 - Ievgen IVANOV <ivanov.eugen@gmail.com>
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


//btdialog_getdarg(tag) - get current value of dynamic argument, associated with dialog
//tag - optional string which identifies dialog
function r=btdialog_getdarg(tag)
r=[]
if argn(2)<1 then
	tag="_btdialog_"
end
h=findobj("tag",tag)
if h<>[] then
	dat=get(h,"userdata")
	r=dat(2)
end
endfunction
