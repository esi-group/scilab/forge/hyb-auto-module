// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) DIGITEO - 2010 - Ievgen IVANOV <ivanov.eugen@gmail.com>
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


//btdialog_getsig(arg, tag) - determine, whether button handler "signalled", i.e.
//requested some operation on data associated with arg, and clear signal
//tag - optional string which identifies dialog
//return value is boolean
function r=btdialog_getsig(arg, tag)
if argn(2)<2 then
	tag="_btdialog_"
end
h=findobj("tag",tag)
r=%f
if h<>[] then
	dat=get(h,"userdata")
	if dat(1)==arg then
		dat(1)=[]
		set(h,"userdata",dat)	//clear "signal"
		r=%t
	end
end
endfunction
