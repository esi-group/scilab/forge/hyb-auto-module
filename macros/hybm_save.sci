// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) DIGITEO - 2010 - Ievgen IVANOV <ivanov.eugen@gmail.com>
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


//hybm_save(fn, dgr) - Save a diagram dgr to a file with name fn 
//(given without path and extension)
function hybm_save(fn, dgr)
if argn(2)<>2 then
	error(msprintf(gettext("%s: Wrong number of input arguments: %d expected.\n"),"hybm_save",2))
end
if type(fn)<>10 then
	error(msprintf(gettext("%s: Wrong type for input argument #%d: Single string expected.\n"),"hybm_save",1));
end
if size(fn,"*")<>1 then
	error(msprintf(gettext("%s: Wrong size for input argument #%d: Single string expected.\n"),"hybm_save",1));
end
if type(dgr)<>17 then
	error(msprintf(gettext("%s: Wrong type for input argument #%d: scs_m structure expected.\n"),"hybm_save",2));
end
scs_m=dgr
propsdoc=scs_m.props.doc
save(TMPDIR+filesep()+fn+".dat",propsdoc) //store doc field separately
if writeXcosDiagram() then
	writeXcosDiagram(TMPDIR+filesep()+fn+".xcos",scs_m)
else
	export_to_hdf5(TMPDIR+filesep()+fn+".h5","scs_m")
end
endfunction
