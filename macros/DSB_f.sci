// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) DIGITEO - 2010 - Ievgen IVANOV <ivanov.eugen@gmail.com>
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


//Interfacing function of hybrid automaton discrete-time state block
function [x,y,typ]=DSB_f(job, arg1, arg2)
x=[]; y=[]; typ=[];
select job
case "plot" then
	standard_draw(arg1)
case "getinputs" then
	[x,y,typ]=standard_inputs(arg1)
case "getoutputs" then
	[x,y,typ]=standard_outputs(arg1)
case "getorigin" then
	[x,y]=standard_origin(arg1)
case "set" then
	x=arg1
	model=arg1.model;graphics=arg1.graphics;label=graphics.exprs
	if size(label,"*")==14 then label(9)=[], end //compatiblity
	while %t do
		[ok,ipc,opc,eqrhs,isinit,tdelta,lab]=..
			scicos_getvalue("Set hybrid automaton''s discrete state parameters",..
				["Number of in-ports";..
				"Number of out-ports";..
				"Difference scheme RHS (column of strings; state varaibles - u1,u2,...)";..
				"Is initial (0/1)";..
				"Time delta"],..
				list("vec",1,"vec",1,"str",1,"vec",1,"vec",1),..
				label);
		if ~ok then break, end
		label=lab
		eqrhs=stripblanks(eqrhs)
		isinit=int(isinit)
		ipc=int(ipc)
		opc=int(opc)
		if ipc<0 then
			messagebox("Number of in-ports should be >=0","modal")
			ok=%f
		end
		if opc<0 then
			messagebox("Number of out-ports should be >=0","modal")
			ok=%f
		end
		if isinit<>0&isinit<>1 then
			messagebox("Is initial should be 0 or 1","modal")
			ok=%f
		end
		if tdelta<=1e-7 then
			messagebox("Time delta should be >1e-7","modal")
			ok=%f
		end
		[eqs,ierr]=evstr(eqrhs)
		if ierr==0 then
			if type(eqs)<>10 then
				messagebox("RHS should be a vector of strings","modal")
				continue
			elseif size(eqs,2)<>1 then
				messagebox("RHS should be a column vector","modal")
				continue
			end
		else
			messagebox("RHS should be a column vector of strings","modal")
			continue
		end
		if ok then
			model.in=ones(ipc,1)
			model.out=ones(opc,1)
			graphics.exprs=label
			arg1.graphics=graphics
			arg1.model=model
			x=arg1
			break
		end
	end
	needcompile=resume(needcompile)
case "define" then
	model=scicos_model()
	model.sim=list("",0)
	model.in=1
	model.out=1
	model.blocktype="d"
	model.dep_ut=[%t %f]
	label=["1";"1";"[''0.9*u1+1'']";"0";"0.1"]
	gr_i=["xstringb(orig(1),orig(2),""State(d)"",sz(1),sz(2),""fill"");"]
	x=standard_define([3 2],model,label,gr_i)
end
endfunction
