// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) DIGITEO - 2010 - Ievgen IVANOV <ivanov.eugen@gmail.com>
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


//Interfacing function of hybrid automaton transition guard block
function [x,y,typ]=TGB_f(job, arg1, arg2)
x=[]; y=[]; typ=[];
select job
case "plot" then
	standard_draw(arg1)
case "getinputs" then
	[x,y,typ]=standard_inputs(arg1)
case "getoutputs" then
	[x,y,typ]=standard_outputs(arg1)
case "getorigin" then
	[x,y]=standard_origin(arg1)
case "set" then
	x=arg1
	graphics=arg1.graphics;label=graphics.exprs
	if size(label,"*")==14 then label(9)=[],end //compatiblity
	while %t do
		[ok,cross,eqtr,lab]=..
			scicos_getvalue("Set hybrid automaton''s transition parameters",..
				["Transition on change of sign (0 means ""- to +"", 1 means ""+ to -"")";..
				"of the value of expression (state variables - u1,u2,...)"],..
				list("vec",1,"str",1),..
				label);
		if ~ok then break, end
		label=lab
		eqtr=stripblanks(eqtr)
		cross=int(cross)
		if cross<>0&cross<>1 then
			messagebox("First parameter should be 0 or 1","modal")
			ok=%f
		end
		if ok then
			graphics.exprs=label
			arg1.graphics=graphics
			x=arg1
			break
		end
	end
	needcompile=resume(needcompile)
case "define" then
	model=scicos_model()
	model.sim=list("",0)
	model.in=1
	model.out=1
	model.blocktype="d"
	model.dep_ut=[%t %f]
	label=["0";"u1+1"]
	gr_i=["xstringb(orig(1),orig(2),""Trans"",sz(1),sz(2),""fill"");"]
	x=standard_define([3 2],model,label,gr_i)
end
endfunction
