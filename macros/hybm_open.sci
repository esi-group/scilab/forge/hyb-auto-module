// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) DIGITEO - 2010 - Ievgen IVANOV <ivanov.eugen@gmail.com>
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


//hybm_open(fn, rw) - Open a file with name fn (given without path and extension) in Xcos
//rw - boolean, indicating whether a user is supposed to edit diagram
function hybm_open(fn, rw)
if argn(2)<>2 then
	error(msprintf(gettext("%s: Wrong number of input arguments: %d expected.\n"),"hybm_open",2))
end
if type(fn)<>10 then
	error(msprintf(gettext("%s: Wrong type for input argument #%d: Single string expected.\n"),"hybm_open",1));
end
if size(fn,"*")<>1 then
	error(msprintf(gettext("%s: Wrong size for input argument #%d: Single string expected.\n"),"hybm_open",1));
end
if type(rw)<>4 then
	error(msprintf(gettext("%s: Wrong type for input argument #%d: Single boolean expected.\n"),"hybm_open",2));
end
if size(rw,"*")<>1 then
	error(msprintf(gettext("%s: Wrong size for input argument #%d: Single boolean expected.\n"),"hybm_open",2));
end
if writeXcosDiagram() then
	xcos(TMPDIR+filesep()+fn+".xcos")
else
	if rw then
		messagebox(["Due to limitations of the current version of this software,",..
		"after editing you should explicitly save diagram under name",..
		TMPDIR+filesep()+fn+".xcos"])
		mdelete(TMPDIR+filesep()+fn+".xcos")
	end
	xcos(TMPDIR+filesep()+fn+".h5")
end
endfunction
